defmodule Babylon.Utils do
  def all_files(path) do
    path
    |> File.ls
    |> filter_files
    |> expand(path)
  end

  defp filter_files({:ok, files}),
    do: Enum.filter(files, fn file -> !String.starts_with?(file, ".") end)

  defp filter_files(error),
    do: error

  defp expand({:error, _}, path),
    do: [path]

  defp expand(files, path),
    do: Enum.flat_map(files, &all_files("#{path}/#{&1}"))
end
