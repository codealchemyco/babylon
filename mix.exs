defmodule Babylon.MixProject do
  use Mix.Project

  def project do
    [
      app: :babylon,
      version: "0.1.2",
      elixir: "~> 1.9",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      name: "Babylon",
      source_url: "https://gitlab.com/codealchemyco/babylon"
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:yaml_elixir, "~> 2.4"},
      {:ex_doc, "~> 0.14", only: :dev, runtime: false}
    ]
  end

  defp description() do
    "A library for detecting programming languages in your project."
  end

  defp package() do
    [
      name: "babylon",
      licenses: ["Apache-2.0"],
      links: %{"GitLab" => "https://gitlab.com/codealchemyco/babylon"}
    ]
  end
end
